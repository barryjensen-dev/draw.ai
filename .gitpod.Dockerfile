# Use the official Python image as a base image
FROM python:3.8-slim

# Set environment variables
ENV DEBIAN_FRONTEND=noninteractive

# Install system dependencies
RUN apt-get update -y && apt-get install -y \
    git \
    curl \
    && apt-get clean

# Install TensorFlow and Keras
RUN pip install tensorflow keras

# Set the default working directory
WORKDIR /workspace

# Start a shell session when the container starts
CMD ["bash"]